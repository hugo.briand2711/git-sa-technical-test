# GIT SA-technical-test



## PAA - Punk Api APP

Cette application utilise l'API Punk pour afficher une liste de bières avec leurs informations détaillées. Elle permet de consulter une liste de bières  et de filtrer les résultats par ABV (Alcohol By Volume).

## Installation


Clonez ce dépôt de code sur votre machine locale.  
Assurez-vous que Node.js et npm sont installés sur votre machine.  
Exécutez npm install pour installer toutes les dépendances.


## Utilisation
Exécutez npm start pour lancer l'application en mode développement.  
Ouvrez votre navigateur à l'adresse http://localhost:3000.  
Parcourez la liste de bières ou filtrez les résultats par ABV > en utilisant les boutons de l'interface utilisateur.


## Fonctionnement du script de facturation

Ce script prend en entrée un fichier JSON(data.json) contenant une liste de membres avec des liens de parenté, et renvoie une liste de membres dans un tableau. Les membres qui doivent être facturés sont ceux afficher dans le tableau "parents" et ceux à ne pas facturer sont ceux dans le tableau "kids". 



## Installation

Assurez-vous que Node.js et npm sont installés sur votre machine.  
Exécutez npm install pour installer toutes les dépendances.

## Utilisation
Utilisez la commande npx ts-node index.ts

import React from "react";
import "./App.css";
import PunkApi from "./components/home/PunkApi";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Grid from "@mui/material/Grid/Grid";
import { Container } from "@mui/material";
import ResponsiveAppBar from "./components/navbar/Nav";
import AllBeers from "./components/beer/AllBeers";
const queryClient = new QueryClient();

const App: React.FC = () => {
  return (
    <div className="App">
      <QueryClientProvider client={queryClient}>
        <Container>
          <Grid container sx={{ height: "100vh" }} spacing={3}>
            <Grid item sx={{ height: "40%" }} xs={12}>
              <ResponsiveAppBar></ResponsiveAppBar>
            </Grid>
            <Routes>
              <Route path="/" element={<AllBeers />} />
              <Route path="/punkapi" element={<PunkApi />} />
            </Routes>
          </Grid>
        </Container>
      </QueryClientProvider>
    </div>
  );
};
export default App;

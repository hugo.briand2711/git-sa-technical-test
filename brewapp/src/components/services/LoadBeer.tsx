import Axios from "axios";
export const LoadBeers = async (filters: number, pageNumber: number) => {
  const res = await Axios.get(
    `https://api.punkapi.com/v2/beers?page=${pageNumber}&per_page=1` +
      (filters > 0 ? `&abv_gt=${filters}` : "")
  );
  return res.data;
};

import React, { useState } from "react";
import { useQuery } from "@tanstack/react-query";
import { LoadBeers } from "../services/LoadBeer";
import Switch from "@mui/material/Switch";
import FormControlLabel from "@mui/material/FormControlLabel";
import Pagination from "@mui/material/Pagination";
import Grid from "@mui/material/Grid/Grid";
import useMediaQuery from '@mui/material/useMediaQuery';

interface Beer {
  id: number;
  name: string;
  tagline: string;
  description: string;
  image_url: string;
  abv: number;
}

const AllBeers = () => {
  const isSmallScreen = useMediaQuery("(max-width: 600px)");
  const [pageNumber, setPageNumber] = useState<number>(1);
  const [filters, setFilteredBeers] = useState<number>(0);
  const [switchChecked, setSwitchChecked] = useState<boolean>(false);
  const {
    isLoading,
    data: beers,
    isError,
    isSuccess,
  } = useQuery(["beers", filters, pageNumber], () =>
    LoadBeers(filters, pageNumber)
  );
  const handleFilterBeers = () => {
    setSwitchChecked(!switchChecked);
    setFilteredBeers(switchChecked ? 0 : 8);
    setPageNumber(1);
  };

  if (isLoading) {
    return <span>Loading...</span>;
  }

  if (isError) {
    return <span>Sorry, an error occurred.</span>;
  }
  return (
    <Grid item sx={{height:'60%'}} xs={12}>
    <div className="container">
      <div className="grid">
        {isSuccess &&
          beers.map((beer: Beer) => (
            <div key={beer.id} className="card">
              <img
                className="card-image"
                src={beer.image_url}
                alt={beer.name}
              />
              <div className="card-content">
                <FormControlLabel 
                  control={
                    <Switch 
                      checked={switchChecked}
                      onChange={handleFilterBeers}
                    />
                  }
                  className="switch-label"
                  label="ABV > 8%"
                />
                <h2 className="card-title">{beer.name}</h2>
                <p className="card-description">{beer.description}</p>
                <p className="card-abv">{beer.abv}%</p>
              </div>
            </div>
          ))}
      </div>
      <div className="pagination-container">
        <Pagination
          count={25}
          page={pageNumber}
          onChange={(event, value) => setPageNumber(value)}
          color="primary"
          className="pagination-text123"
          size={isSmallScreen ? "small" : "medium"}

        />
      </div>
    </div>
  </Grid>
  );
};

export default AllBeers;

import React from "react";
import Grid from "@mui/material/Grid/Grid";
import { Button } from "@mui/material";
const PunkApi: React.FC = () => {
  return (
    <Grid item sx={{ height: "60%" }} xs={12}>
      <div className="punk-api-container">
        <h1>Punk API</h1>
        <p>
          Punk API is a free-to-use API that provides data on a wide variety of
          beers. It was created by the BrewDog brewery and includes information
          on beer names, descriptions, alcohol content, and more. The API is
          accessible through a RESTful interface and returns data in JSON
          format.
        </p>
        <p>
          This component could be used to display data fetched from the Punk
          API, such as a list of beers or information about a specific beer. The
          data could be retrieved using a library like Axios or through a state
          management tool like Redux.{" "}
        </p>
        <Button
          variant="contained"
          href="https://punkapi.com/documentation/v2"
          target="_blank"
          rel="noopener"
        >
          Go to Punk API
        </Button>
      </div>
    </Grid>
  );
};

export default PunkApi;

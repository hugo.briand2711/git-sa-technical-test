interface Member {
  id: number;
  name: string;
  linkId: number | null;
}

function getData(data: Member[]): { parents: Member[]; kids: Member[] } {
  const parents: Member[] = [];
  const kids: Member[] = [];

  const ViewedMember: Set<Member> = new Set();

  function filterMember(member: Member, parent: Member | null) {
    if (ViewedMember.has(member)) {
      return;
    }

    ViewedMember.add(member);

    if (member.linkId === null) {
      parents.push(member);

      for (const child of data.filter((m) => m.linkId === member.id)) {
        filterMember(child, member);
      }
    } else {
      kids.push(member);

      const targetMember = data.find((m) => m.id === member.linkId);

      if (targetMember) {
        filterMember(targetMember, null);
      }
    }
  }

  for (const member of data) {
    filterMember(member, null);
  }

  return { parents, kids };
}

export { getData };
